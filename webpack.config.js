const path = require('path');
const resolve = require('path').resolve;
const webpack = require('webpack');

module.exports = {
  entry: {
    app: resolve('./src/index.js')
  },

  output: {
    path: path.join(__dirname, './dist'),
    filename: 'bundle.js',
  },

  devServer: {
    https: true
  },

  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: [/node_modules/],
        query: {
            presets: ['env', 'stage-0', 'react']
        }
      }
    ]
  }
}
