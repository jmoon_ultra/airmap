import React, { Component } from 'react';
import { connect } from 'react-redux';

class Layer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isHidden: true
		}
	}

	toggleHidden() {
		this.setState({
			isHidden: !this.state.isHidden
		})
	}

	render() {
		const trimmed = this.props.airmapData.filter(item => item !== null );

		const list = trimmed.map((item, index) => {
			return <li key={index}>{item['data']}</li>
		})

		return (
			<div className="Layer" id="js-layer">
				<button className="btn btn-primary" onClick={this.toggleHidden.bind(this)}>TOGGLE DATA</button>
			 	 <ul id="list" className={!this.state.isHidden ? 'Layer-list show' : 'Layer-list'}>{list}</ul>
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		airmapData: state.airmapData
	};
}

export default connect(mapStateToProps)(Layer);