import React, { Component } from 'react';
import { connect } from 'react-redux';
import { mapboxKey } from '../../config';

class Map extends Component {

	componentDidMount(){
		const { longitude, latitude } = this.props;
		const token = mapboxKey;
		const zoom = 15;

		const mapConfig = {
			container: 'js-map',
			style: 'mapbox://styles/mapbox/streets-v9',
			center: [longitude, latitude],
			zoom: zoom,
		};

		mapboxgl.accessToken = token;

		this.map = new mapboxgl.Map(mapConfig);

		this.map.addControl(new MapboxGeocoder({
				accessToken: mapboxgl.accessToken
		}));

		// this.map.addControl(new mapboxgl.GeolocateControl({
		// 		positionOptions: {
		// 				enableHighAccuracy: true
		// 		},
		// 		trackUserLocation: true
		// }));

		this.map.on('load', (e) => {
			// 
		});

		// this.map.getUserLocation().then(
		// 	function(userLocation) {
		// 		console.log("Current user location: " +  userLocation.location.lat + ", " + userLocation.location.lng);
		// 	}
		// )

	}

	render(){
		return (
			<div className='Map' id="js-map"></div>
		);
	}
}

const mapStateToProps = state => {
	return {
		longitude: state.position.longitude,
		latitude: state.position.latitude
	};
}

export default connect(mapStateToProps)(Map);