import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import C from './constants';
import storeFactory from './store';
import reducers from './reducers';
import { changePosition, fetchAirmapData } from './actions';
import App from './components/app';

const initState = (localStorage['airmap-store']) ? JSON.parse(localStorage['airmap-store']) : {};

const store = storeFactory(initState);

store.subscribe(() => {
	const state = JSON.stringify(store.getState());
	localStorage['airmap-store'] = state;
})

const getInitPosition = options => {
	store.dispatch({
		type: C.FETCH_CURRENT_POS
	});

	return new Promise((resolve, reject) => {
		navigator.geolocation.getCurrentPosition(resolve, reject, options);
	});
}

getInitPosition()
	.then((position) => {
		store.dispatch(
			changePosition({
				longitude: position.coords.longitude, 
				latitude: position.coords.latitude
			})
		);

		store.dispatch(
			fetchAirmapData({
				longitude: position.coords.longitude, 
				latitude: position.coords.latitude
			})
		);

		ReactDOM.render(
		<Provider store={ store }>
			<App />
		</Provider>,document.getElementById('main'));
	})
	.catch((err) => {
		console.error(err.message);
	});