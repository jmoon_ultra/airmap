import { combineReducers } from 'redux';
import C from '../constants';

export const position = (state = {}, action) => {

	switch (action.type) {
		case C.CHANGE_POSITION:
			return action.payload
		default:
			return state;
	}

}

export const airmapData = (state = [], action) => {

	switch (action.type) {
		case C.ADD_AIRMAP_DATA:
			return [...state, action.payload]
		default:
			return state;
	}
	
}

export const fetching = (state = false, action) => {

	switch (action.type) {
		case C.FETCH_CURRENT_POS:
		case C.FETCH_AIRMAP_DATA:
			return true;
		case C.CANCEL_FETCHING:
			return false;
		case C.CHANGE_POSITION:
			return false;
		default:
			return state;
	}

}

export default combineReducers({
	position,
	airmapData,
	fetching
});
