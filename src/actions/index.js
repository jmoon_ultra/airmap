import C from '../constants';
import axios from 'axios';
import { airmapKey } from '../../config';

export const changePosition = position => (
	{
		type: C.CHANGE_POSITION,
		payload: position
	}
);

export const fetchAirmapData = value => dispatch => {

	// TODO: MAKE THE PARAMS DYNAMIC ... AND MUCH MORE.
	axios.get(`https://api.airmap.com/tiledata/v1/usa_part_107/controlled_airspace,laanc,non_geo/12/699/1635?apikey=${airmapKey}`)
		.then(response => {
			console.log('AIRMAP RESPONSE: ', response);
			dispatch({
				type: C.ADD_AIRMAP_DATA,
				payload: response
			})
		})
		.catch(error => {
			console.log('ERROR: ', error);
		})
}