import C from '../constants';
import reducers from '../reducers';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';

const consoleMessages = store => next => action => {

	let result;

	console.groupCollapsed(`dispatching action => ${action.type}`);

	result = next(action);

	let { position } = store.getState();

	console.log(`
		CURRENT POSITION: ${position}
	`)

	console.groupEnd();

	return result;

}

export default (initState = {}) => {
	return applyMiddleware(thunk, consoleMessages)(createStore)(reducers, initState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
}