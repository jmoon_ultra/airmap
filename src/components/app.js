import React, { Component } from 'react';
import Map from '../containers/Map';
import Layer from '../containers/Layer';

export default class App extends Component {
	render() {
		return (
			<div>
				<Layer/>
				<Map/>
			</div>
		);
	}
}